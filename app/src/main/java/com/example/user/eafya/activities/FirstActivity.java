package com.example.user.eafya.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.opengl.Visibility;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.user.eafya.R;

public class FirstActivity extends AppCompatActivity {

    Button signin;
    Button register;
    TextView eafya;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_firstactivity);


        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setLogo(R.drawable.logo);
        getSupportActionBar().setDisplayUseLogoEnabled(true);






        //setting font for the signin button
        signin = (Button) findViewById(R.id.sign_in);
        Typeface myCustomFont = Typeface.createFromAsset(getAssets(), "fonts/Raleway-Light.ttf");
        signin.setTypeface(myCustomFont);

        //setting font for the signin button
        register = (Button) findViewById(R.id.register);
        Typeface registerCustomFont = Typeface.createFromAsset(getAssets(), "fonts/Raleway-Light.ttf");
        register.setTypeface(registerCustomFont);

        //setting font for the E afya text
        eafya = (TextView) findViewById(R.id.eafya);
        Typeface eafyaCustomFont = Typeface.createFromAsset(getAssets(), "fonts/Raleway-Light.ttf");
        eafya.setTypeface(registerCustomFont);

    }


    //adding an event for when the Sign In button is clicked
    public void goToLogin(View view) {
        startActivity(new Intent(this,LoginActivity.class));


    }

    //adding an event for when we click the FirstActivity Button
    public void goToRegister(View view) {
        startActivity(new Intent(this,RegisterActivity.class));


    }


    public void overrideFonts(final Context context, final View v) {
        try {
            if (v instanceof ViewGroup) {
                ViewGroup vg = (ViewGroup) v;
                for (int i = 0; i < vg.getChildCount(); i++) {
                    View child = vg.getChildAt(i);
                    overrideFonts(context, child);
                }
            } else if (v instanceof TextView ) {
                ((TextView) v).setTypeface(Typeface.createFromAsset(context.getAssets(), "fonts/Raleway-Light.ttf"));
            }
        } catch (Exception e) {
        }
    }






}

