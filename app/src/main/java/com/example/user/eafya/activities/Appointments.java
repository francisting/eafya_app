package com.example.user.eafya.activities;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;

import com.example.user.eafya.R;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class Appointments extends AppCompatActivity {

    Spinner Spinner;
    EditText date, time;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_appointments);

        overrideFonts(this, findViewById(android.R.id.content));




        //specialist spinner

        List<String> spinnerArray =  new ArrayList<String>();
        spinnerArray.add("Orthopaedic");
        spinnerArray.add("Dentist");

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                this, android.R.layout.simple_spinner_item, spinnerArray);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        Spinner sItems = (Spinner) findViewById(R.id.specialization_spinner);
        sItems.setAdapter(adapter);


        //counties spinner

        List<String> countiesSpinnerArray =  new ArrayList<String>();
        countiesSpinnerArray.add("Mombasa");
        countiesSpinnerArray.add("Nairobi");

        ArrayAdapter<String> county = new ArrayAdapter<String>(
                this, android.R.layout.simple_spinner_item, countiesSpinnerArray);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        Spinner counties = (Spinner) findViewById(R.id.county_spinner);
        counties.setAdapter(county);

        //hospital Spinner
        List<String> hospitalSpinnerArray =  new ArrayList<String>();
        hospitalSpinnerArray.add("KNH");
        hospitalSpinnerArray.add("Agha Khan");
        hospitalSpinnerArray.add("Mater Hospital");
        hospitalSpinnerArray.add("Kiambu level 5");

        ArrayAdapter<String> hospital = new ArrayAdapter<String>(
                this, android.R.layout.simple_spinner_item, hospitalSpinnerArray);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        Spinner hospitals = (Spinner) findViewById(R.id.hospital_spinner);
        hospitals.setAdapter(hospital);


        //doctors spinner
        List<String> doctorSpinnerArray =  new ArrayList<String>();
        doctorSpinnerArray.add("Millicent Wanjeri");
        doctorSpinnerArray.add("Francis Kamau");
        doctorSpinnerArray.add("Georgiadis Shilisia");
        doctorSpinnerArray.add("Sylvester Tamba Angulu");

        ArrayAdapter<String> doctorAdapter = new ArrayAdapter<String>(
                this, android.R.layout.simple_spinner_item, doctorSpinnerArray);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        Spinner doctors = (Spinner) findViewById(R.id.doctor_spinner);
        doctors.setAdapter(doctorAdapter);
        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++//
        //This code is time picker



        time =(EditText) findViewById(R.id.et_select_time);

        time.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                final Calendar c = Calendar.getInstance();
                int hour = c.get(Calendar.HOUR_OF_DAY);
                int minute = c.get(Calendar.MINUTE);


                TimePickerDialog timePickerDialog = new TimePickerDialog(Appointments.this,
                        new TimePickerDialog.OnTimeSetListener() {

                            @Override
                            public void onTimeSet(TimePicker view, int hourOfDay,
                                                  int minute) {

                                time.setText(hourOfDay + ":" + minute);
                            }
                        }, hour, minute, false);
                timePickerDialog.show();
            }

        });






//This code is for populating the edit text with the date from the date picker dialog
        date =(EditText) findViewById(R.id.et_select_date);


        date.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                showDialog(DATE_DIALOG_ID);
            }

        });
        final Calendar calendar = Calendar.getInstance();
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        if (date.getText().toString() != null) {
            try {
                calendar.setTime(df.parse(date.getText().toString()));
            } catch (java.text.ParseException e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
            }
            mYear = calendar.get(Calendar.YEAR);
            mMonth = calendar.get(Calendar.MONTH);
            mDay = calendar.get(Calendar.DAY_OF_MONTH);
            SimpleDateFormat month_date = new SimpleDateFormat("MMM");
            month = month_date.format(calendar.getTime());
        } else {
            mYear = calendar.get(Calendar.YEAR);
            mMonth = calendar.get(Calendar.MONTH);
            mDay = calendar.get(Calendar.DAY_OF_MONTH);
            SimpleDateFormat month_date = new SimpleDateFormat("MMM");
            month = month_date.format(calendar.getTime());
        }



    }



    static final int DATE_DIALOG_ID = 1;
    private int mYear;
    private int mMonth;
    private int mDay;
    private String month;
    private String dateOfBirth;


    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case DATE_DIALOG_ID:
                return new DatePickerDialog(this, mDateSetListener, mYear, mMonth,
                        mDay);
        }
        return null;
    }

    @Override
    protected void onPrepareDialog(int id, Dialog dialog) {
        switch (id) {
            case DATE_DIALOG_ID:
                ((DatePickerDialog) dialog).updateDate(mYear, mMonth, mDay);
                break;
        }
    }

    private DatePickerDialog.OnDateSetListener mDateSetListener = new DatePickerDialog.OnDateSetListener() {

        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            mYear = year;
            mMonth = monthOfYear;
            mDay = dayOfMonth;

            String dateSetter = (new StringBuilder().append(mYear).append("-")
                    .append(mMonth + 1).append("-").append(mDay).append(""))
                    .toString();
            final Calendar cal = Calendar.getInstance();
            DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
            if (dateSetter != null) {
                try {
                    cal.setTime(df.parse(dateSetter));
                } catch (java.text.ParseException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }
                SimpleDateFormat month_date = new SimpleDateFormat("MMM");
                month = month_date.format(cal.getTime());
            }
            updateDisplay();


        }
    };

    private void updateDisplay() {
        dateOfBirth = (new StringBuilder()
                // Month is 0 based so add 1
                .append(mYear).append("-").append(mMonth + 1).append("-")
                .append(mDay).append("")).toString();
        date.setText(new StringBuilder()
                // Month is 0 based so add 1
                .append(mDay).append("-").append(month).append("-")
                .append(mYear));
    }


        public void overrideFonts(final Context context, final View v) {
            try {
                if (v instanceof ViewGroup) {
                    ViewGroup vg = (ViewGroup) v;
                    for (int i = 0; i < vg.getChildCount(); i++) {
                        View child = vg.getChildAt(i);
                        overrideFonts(context, child);
                    }
                } else if (v instanceof TextView) {
                    ((TextView) v).setTypeface(Typeface.createFromAsset(context.getAssets(), "fonts/Raleway-Light.ttf"));
                }
            } catch (Exception e) {
            }
        }
    }


