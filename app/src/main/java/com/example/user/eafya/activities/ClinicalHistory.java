package com.example.user.eafya.activities;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.user.eafya.Configs.Configs;
import com.example.user.eafya.R;
import com.example.user.eafya.adapters.ClinicalHistoryAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


public class ClinicalHistory extends AppCompatActivity {

    RecyclerView news_RV;
    SwipeRefreshLayout swipeRefreshLayout;
    List<com.example.user.eafya.models.ClinicalHistory> clinicalHistoryList;
    com.example.user.eafya.models.ClinicalHistory clinicalHistory;
    ClinicalHistoryAdapter clinicalHistoryAdapter;
    private StringRequest newsRequest;
    private LinearLayout no_data;
    private LinearLayoutManager linearLayoutManager;
    private RequestQueue requestQueue;
    private ProgressDialog pb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tips);
        initWidgets();

        fetchData();
    }

    private void fetchData() {
        pb = new ProgressDialog(this);
        pb.setIcon(R.mipmap.ic_launcher);
        pb.setTitle("Please wait");
        pb.setCancelable(false);
        pb.setMessage("Loading data..");
        pb.show();
        newsRequest = new StringRequest(Request.Method.GET, Configs.NEWS_REQ_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String result) {
                pb.dismiss();
                try {
                    JSONObject jsonObject = new JSONObject(result);
                    JSONArray jsonArray = jsonObject.getJSONArray("news");
                    Log.d("maera: ", String.valueOf(jsonArray.length()));

                    if(jsonArray.length()<1) {
                        clinicalHistoryList.clear();
                        no_data.setVisibility(View.VISIBLE);

                    }else if( jsonArray.length()>0){
                        if(no_data.getVisibility()==View.VISIBLE){
                            no_data.setVisibility(View.GONE);
                        }

                        for(int i=0 ; i<jsonArray.length() ; i++){
                            JSONObject data = jsonArray.getJSONObject(i);
                            clinicalHistory = new com.example.user.eafya.models.ClinicalHistory();
                            clinicalHistory.setDate(data.getString("news_date"));
                            clinicalHistory.setTitle(data.getString("news_title"));
                            clinicalHistory.setDescription(data.getString("news_desc"));
                            clinicalHistory.setImg(data.getString("news_img"));

                            clinicalHistoryList.add(clinicalHistory);
                        }
                        clinicalHistoryAdapter = new ClinicalHistoryAdapter(ClinicalHistory.this, clinicalHistoryList);

                        linearLayoutManager = new LinearLayoutManager(ClinicalHistory.this);

                        news_RV.setLayoutManager(linearLayoutManager);
                        news_RV.setItemAnimator(new DefaultItemAnimator());
                        news_RV.setAdapter(clinicalHistoryAdapter);
                        news_RV.setSaveEnabled(true);
                        news_RV.setSaveFromParentEnabled(true);
                        clinicalHistoryAdapter.notifyDataSetChanged();
                    }


                } catch (JSONException e) {

                    Log.d("MaeraJ",e.toString());

                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                pb.dismiss();
                AlertDialog.Builder dialog = new AlertDialog.Builder(ClinicalHistory.this);
                dialog.setTitle("Something went wrong !!");
                dialog.setCancelable(true);
                dialog.setIcon(R.mipmap.ic_launcher);
                dialog.setMessage("Check your data connection");
                dialog.setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        //retry
                        dialogInterface.dismiss();
                        fetchData();
                    }
                });
                dialog.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        //cancel
                        dialogInterface.dismiss();

                    }
                });
                dialog.create();

                dialog.show();
                Log.d("Maera",volleyError.toString());
            }
        });
        requestQueue.add(newsRequest);

    }

    private void initWidgets() {
        requestQueue = Volley.newRequestQueue(this);
        no_data = findViewById(R.id.nodata_LY);
        news_RV = findViewById(R.id.news_RV);
        clinicalHistoryList = new ArrayList<>();
        swipeRefreshLayout = findViewById(R.id.swipeContainer);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refresh();
            }

        });
    }

    private void refresh() {
        try {

            clinicalHistoryList.clear();

            fetchData();
            swipeRefreshLayout.setRefreshing(false);
        } catch (Exception e) {
            e.printStackTrace();

        }
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()){
            case R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);

    }
}
