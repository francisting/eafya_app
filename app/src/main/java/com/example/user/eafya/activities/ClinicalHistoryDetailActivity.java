package com.example.user.eafya.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.user.eafya.R;


public class ClinicalHistoryDetailActivity extends AppCompatActivity {
    ImageView img;
    TextView title,description, date;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tips_detail);
        initWidgets();
    }

    private void initWidgets() {
        img = findViewById(R.id.newsdetail_image_IV);
        title = findViewById(R.id.newsdetail_title__TV);
        description = findViewById(R.id.newsdetail_desc_TV);
        date = findViewById(R.id.newsdetail_date_TV);
        try {
            attach();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void attach() {
        title.setText(getIntent().getStringExtra("news_title"));
        date.setText(getIntent().getStringExtra("news_date"));
        description.setText(getIntent().getStringExtra("news_description"));

        Glide.with(this).load(getIntent().getStringExtra("news_img")).placeholder(R.drawable.loader).into(img);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()){
            case R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);

    }
}
