package com.example.user.eafya.activities;
//
//import android.app.ProgressDialog;
//import android.content.Context;
//import android.content.Intent;
//import android.graphics.Typeface;
//import android.os.AsyncTask;
//import android.support.v7.app.AppCompatActivity;
//import android.os.Bundle;
//import android.text.TextUtils;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.Button;
//import android.widget.EditText;
//import android.widget.TextView;
//import android.widget.Toast;
//
//import com.example.user.eafya.R;
//
//import java.util.HashMap;
//
//public class RegisterActivity extends AppCompatActivity {
//
//    Button register, log_in;
//    EditText First_Name, Last_Name, Email, Password ;
//    String F_Name_Holder, L_Name_Holder, EmailHolder, PasswordHolder;
//    String finalResult ;
//    String HttpURL = "http://192.168.0.31:9090/ApiController";
//    Boolean CheckEditText ;
//    ProgressDialog progressDialog;
//    HashMap<String,String> hashMap = new HashMap<>();
//    HttpParse httpParse = new HttpParse();
//
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_register);
//
//
//        overrideFonts(this, findViewById(android.R.id.content));
//
//        //Assign Id'S
//        First_Name = (EditText)findViewById(R.id.firstname);
//        Last_Name = (EditText)findViewById(R.id.lastname);
//        Email = (EditText)findViewById(R.id.email);
//        Password = (EditText)findViewById(R.id.password);
//
//        register = (Button)findViewById(R.id.register);
//        log_in = (Button)findViewById(R.id.log_in);
//
//        //Adding Click Listener on button.
//        register.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                // Checking whether EditText is Empty or Not
//                CheckEditTextIsEmptyOrNot();
//
//                if(CheckEditText){
//
//                    // If EditText is not empty and CheckEditText = True then this block will execute.
//
//                    UserRegisterFunction(F_Name_Holder,L_Name_Holder, EmailHolder, PasswordHolder);
//                    Toast.makeText(RegisterActivity.this, "Sending data.", Toast.LENGTH_LONG).show();
//
//
//
//                }
//                else {
//
//                    // If EditText is empty then this block will execute .
//                    Toast.makeText(RegisterActivity.this, "Please fill all form fields.", Toast.LENGTH_LONG).show();
//
//                }
//
//
//            }
//        });
//
//        log_in.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                Intent intent = new Intent(RegisterActivity.this,LoginActivity.class);
//                startActivity(intent);
//
//            }
//        });
//
//    }
//
//    public void CheckEditTextIsEmptyOrNot(){
//
//        F_Name_Holder = First_Name.getText().toString();
//        L_Name_Holder = Last_Name.getText().toString();
//        EmailHolder = Email.getText().toString();
//        PasswordHolder = Password.getText().toString();
//
//
//        if(TextUtils.isEmpty(F_Name_Holder) || TextUtils.isEmpty(L_Name_Holder) || TextUtils.isEmpty(EmailHolder) || TextUtils.isEmpty(PasswordHolder))
//        {
//
//            CheckEditText = false;
//
//        }
//        else {
//
//            CheckEditText = true ;
//        }
//
//    }
//
//
//
//
//    public void overrideFonts(final Context context, final View v) {
//        try {
//            if (v instanceof ViewGroup) {
//                ViewGroup vg = (ViewGroup) v;
//                for (int i = 0; i < vg.getChildCount(); i++) {
//                    View child = vg.getChildAt(i);
//                    overrideFonts(context, child);
//                }
//            } else if (v instanceof TextView) {
//                ((TextView) v).setTypeface(Typeface.createFromAsset(context.getAssets(), "fonts/Raleway-Light.ttf"));
//            }
//        } catch (Exception e) {
//        }
//    }
//
//
//    public void UserRegisterFunction(final String F_Name, final String L_Name, final String email, final String password){
//
//        class UserRegisterFunctionClass extends AsyncTask<String,Void,String> {
//
//            @Override
//            protected void onPreExecute() {
//                super.onPreExecute();
//
//                progressDialog = ProgressDialog.show(RegisterActivity.this,"Loading Data",null,true,true);
//            }
//
//            @Override
//            protected void onPostExecute(String httpResponseMsg) {
//
//                super.onPostExecute(httpResponseMsg);
//
//                progressDialog.dismiss();
//
//                Toast.makeText(RegisterActivity.this,httpResponseMsg.toString(), Toast.LENGTH_LONG).show();
//
//            }
//
//            @Override
//            protected String doInBackground(String... params) {
//
//                hashMap.put("firstname",params[0]);
//
//                hashMap.put("lastname",params[1]);
//
//                hashMap.put("email",params[2]);
//
//                hashMap.put("password",params[3]);
//
//                finalResult = httpParse.postRequest(hashMap, HttpURL);
//
//                return finalResult;
//            }
//        }
//
//        UserRegisterFunctionClass userRegisterFunctionClass = new UserRegisterFunctionClass();
//
//        userRegisterFunctionClass.execute(F_Name,L_Name,email,password);
//    }
//
//}
//
//


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request.Method;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import com.example.user.eafya.Configs.Configs;
import com.example.user.eafya.R;
import com.example.user.eafya.activities.AppController;
import com.example.user.eafya.activities.LoginActivity;
import com.example.user.eafya.activities.MainActivity;
import com.example.user.eafya.activities.SQLiteHandler;
import com.example.user.eafya.activities.SessionManager;


public class RegisterActivity extends Activity {
    private static final String TAG = RegisterActivity.class.getSimpleName();
    private Button btnRegister;
    private Button btnLinkToLogin;
    private EditText inputFirstName;
    private EditText inputLastName;
    private EditText inputEmail;
    private EditText inputPassword;
    private ProgressDialog pDialog;
    private SessionManager session;
    private SQLiteHandler db;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        inputFirstName = (EditText) findViewById(R.id.firstname);
        inputLastName = (EditText) findViewById(R.id.lastname);
        inputPassword = (EditText) findViewById(R.id.password);
        inputEmail = (EditText) findViewById(R.id.email);
        btnRegister = (Button) findViewById(R.id.btn_register);
        btnLinkToLogin = (Button) findViewById(R.id.btnLinkToLoginScreen);

        // Progress dialog
        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);

        // Session manager
        session = new SessionManager(getApplicationContext());

        // SQLite database handler
        db = new SQLiteHandler(getApplicationContext());

        // Check if user is already logged in or not
        if (session.isLoggedIn()) {
            // User is already logged in. Take him to main activity
            Intent intent = new Intent(RegisterActivity.this,
                    MainActivity.class);
            startActivity(intent);
            finish();
        }

        // Register Button Click event
        btnRegister.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                String firstname = inputFirstName.getText().toString().trim();
                String lastname = inputLastName.getText().toString().trim();

                String email = inputEmail.getText().toString().trim();
                String password = inputPassword.getText().toString().trim();

                if (!firstname.isEmpty() && !lastname.isEmpty() && !email.isEmpty() && !password.isEmpty()) {
                    registerUser(firstname, lastname, email, password);
                } else {
                    Toast.makeText(getApplicationContext(),
                            "Please enter your details!", Toast.LENGTH_LONG)
                            .show();
                }
            }
        });

        // Link to Login Screen
        btnLinkToLogin.setOnClickListener(new View.OnClickListener() {

            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(),
                        LoginActivity.class);
                startActivity(i);
                finish();
            }
        });

    }

    /**
     * Function to store user in MySQL database will post params(tag, name,
     * email, password) to activity_register url
     */
    private void registerUser(final String firstname, final String lastname, final String email,
                              final String password) {
        // Tag used to cancel the request
        String tag_string_req = "req_register";

        pDialog.setMessage("Registering ...");
        showDialog();

        StringRequest strReq = new StringRequest(Method.POST,
                Configs.URL_LOGIN, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Register Response: " + response.toString());
                hideDialog();

                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");
                    if (!error) {
                        // User successfully stored in MySQL
                        // Now store the user in sqlite
                        String uid = jObj.getString("uid");

                        JSONObject user = jObj.getJSONObject("user");
                        String firstname = user.getString("firstname");
                        String lastname = user.getString("lastname");
                        String email = user.getString("email");
                        String password = user.getString("password");
                       // String created_at = user
                                //.getString("created_at");

                        // Inserting row in users table
                        db.addUser(firstname,lastname,email,password);

                        Toast.makeText(getApplicationContext(), "User successfully registered. Try login now!", Toast.LENGTH_LONG).show();

                        // Launch login activity
                        Intent intent = new Intent(
                                RegisterActivity.this,
                                LoginActivity.class);
                        startActivity(intent);
                        finish();
                    } else {

                        // Error occurred in registration. Get the error
                        // message
                        String errorMsg = jObj.getString("error_msg");
                        Toast.makeText(getApplicationContext(),
                                errorMsg, Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Registration Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(),
                        error.getMessage(), Toast.LENGTH_LONG).show();
                hideDialog();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting params to activity_register url
                Map<String, String> params = new HashMap<String, String>();
                params.put("firstname", firstname);
                params.put("lastname", lastname);
                params.put("email", email);
                params.put("password", password);
                params.put("action", "register");

                return params;
            }

        };

        // Adding request to request queue

        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }
}

//import android.content.Intent;
//import android.graphics.Typeface;
//import android.support.v7.app.AppCompatActivity;
//import android.os.Bundle;
//import android.view.View;
//import android.widget.Button;
//
//import com.example.user.eafya.R;
//
//public class RegisterActivity extends AppCompatActivity {
//
//    Button fblogin;
//    Button googleplus_login;
//    Button next;
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_register);
//
//        //setting font for the fblogin button
//        fblogin = (Button) findViewById(R.id.fb_login);
//        Typeface fbloginCustomFont = Typeface.createFromAsset(getAssets(), "fonts/Raleway-Light.ttf");
//        fblogin.setTypeface(fbloginCustomFont);
//
//        //setting font for the googleplus button
//        googleplus_login = (Button) findViewById(R.id.google_pluslogin);
//        Typeface googleplusCustomFont = Typeface.createFromAsset(getAssets(), "fonts/Raleway-Light.ttf");
//        googleplus_login.setTypeface(googleplusCustomFont);
//
////        next = (Button) findViewById(R.id.register);
////        Typeface nextCustomFont = Typeface.createFromAsset(getAssets(), "fonts/Raleway-Light.ttf");
////        next.setTypeface(nextCustomFont);
//
//
//
//
//
//    }
//
//    public void goToMain(View view) {
//        startActivity(new Intent(this,MainActivity.class));
//
//
//    }
//
//}
