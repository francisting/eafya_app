package com.example.user.eafya.activities;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.user.eafya.R;
import com.example.user.eafya.adapters.MedicalTipsAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import com.example.user.eafya.Configs.Configs;





public class MedicalTips extends AppCompatActivity {

    RecyclerView news_RV;
    SwipeRefreshLayout swipeRefreshLayout;
    List<com.example.user.eafya.models.MedicalTips> medicalTipsList;
    com.example.user.eafya.models.MedicalTips medicalTips;
    MedicalTipsAdapter medicalTipsAdapter;
    private StringRequest newsRequest;
    private LinearLayout no_data;
    private LinearLayoutManager linearLayoutManager;
    private RequestQueue requestQueue;
    private ProgressDialog pb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tips);
        initWidgets();

        fetchData();
    }

    private void fetchData() {
        pb = new ProgressDialog(this);
        pb.setIcon(R.mipmap.ic_launcher);
        pb.setTitle("Please wait");
        pb.setCancelable(false);
        pb.setMessage("Loading data..");
        pb.show();
        newsRequest = new StringRequest(Request.Method.GET, Configs.NEWS_REQ_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String result) {
                pb.dismiss();
                try {
                    JSONObject jsonObject = new JSONObject(result);
                    JSONArray jsonArray = jsonObject.getJSONArray("news");
                    Log.d("maera: ", String.valueOf(jsonArray.length()));

                    if(jsonArray.length()<1) {
                        medicalTipsList.clear();
                        no_data.setVisibility(View.VISIBLE);

                    }else if( jsonArray.length()>0){
                        if(no_data.getVisibility()==View.VISIBLE){
                            no_data.setVisibility(View.GONE);
                        }

                        for(int i=0 ; i<jsonArray.length() ; i++){
                            JSONObject data = jsonArray.getJSONObject(i);
                            medicalTips = new com.example.user.eafya.models.MedicalTips();
                            medicalTips.setDate(data.getString("news_date"));
                            medicalTips.setTitle(data.getString("news_title"));
                            medicalTips.setDescription(data.getString("news_desc"));
                            medicalTips.setImg(data.getString("news_img"));

                            medicalTipsList.add(medicalTips);
                        }
                        medicalTipsAdapter = new MedicalTipsAdapter(MedicalTips.this, medicalTipsList);

                        linearLayoutManager = new LinearLayoutManager(MedicalTips.this);

                        news_RV.setLayoutManager(linearLayoutManager);
                        news_RV.setItemAnimator(new DefaultItemAnimator());
                        news_RV.setAdapter(medicalTipsAdapter);
                        news_RV.setSaveEnabled(true);
                        news_RV.setSaveFromParentEnabled(true);
                        medicalTipsAdapter.notifyDataSetChanged();
                    }


                } catch (JSONException e) {

                    Log.d("MaeraJ",e.toString());

                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                pb.dismiss();
                AlertDialog.Builder dialog = new AlertDialog.Builder(MedicalTips.this);
                dialog.setTitle("Something went wrong !!");
                dialog.setCancelable(true);
                dialog.setIcon(R.mipmap.ic_launcher);
                dialog.setMessage("Check your data connection");
                dialog.setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        //retry
                        dialogInterface.dismiss();
                        fetchData();
                    }
                });
                dialog.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        //cancel
                        dialogInterface.dismiss();

                    }
                });
                dialog.create();

                dialog.show();
                Log.d("Maera",volleyError.toString());
            }
        });
        requestQueue.add(newsRequest);

    }

    private void initWidgets() {
        requestQueue = Volley.newRequestQueue(this);
        no_data = findViewById(R.id.nodata_LY);
        news_RV = findViewById(R.id.news_RV);
        medicalTipsList = new ArrayList<>();
        swipeRefreshLayout = findViewById(R.id.swipeContainer);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refresh();
            }

        });
    }

    private void refresh() {
        try {

            medicalTipsList.clear();

            fetchData();
            swipeRefreshLayout.setRefreshing(false);
        } catch (Exception e) {
            e.printStackTrace();

        }
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()){
            case R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);

    }
}
