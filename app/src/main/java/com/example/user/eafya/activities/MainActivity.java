package com.example.user.eafya.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.user.eafya.R;

public class MainActivity extends AppCompatActivity {

    TextView patientName;
    TextView patientId;
    TextView totalVisitslabel;
    TextView totalVisitsreal;
    TextView nextapointmentlabel;
    TextView nextAppointmentReal;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        overrideFonts(this, findViewById(android.R.id.content));
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setLogo(R.drawable.logo);
        getSupportActionBar().setDisplayUseLogoEnabled(true);


        totalVisitslabel = (TextView) findViewById(R.id.totalvisitslabel);
        Typeface totalVisitslabelCustomFont = Typeface.createFromAsset(getAssets(), "fonts/Raleway-Bold.ttf");
        totalVisitslabel.setTypeface(totalVisitslabelCustomFont);
;



        nextapointmentlabel = (TextView) findViewById(R.id.nextappointmentlabel);
        Typeface nextapointmentlabelCustomFont = Typeface.createFromAsset(getAssets(), "fonts/Raleway-Bold.ttf");
        nextapointmentlabel.setTypeface(nextapointmentlabelCustomFont);



        }


    public void goToAppointment(View view) {
        startActivity(new Intent(this,Appointments.class));


    }

    public void goToAllergies(View view) {
        startActivity(new Intent(this,Allergies.class));


    }

    public void goToWallet(View view) {
        startActivity(new Intent(this,Wallet.class));


    }

    public void goToAmbulance(View view) {
        startActivity(new Intent(this,MapsActivityCurrentPlace.class));


    }

    public void goToTips(View view) {
        startActivity(new Intent(this, MedicalTips.class));
    }

    public void goToClinicalHistory(View view) {
        startActivity(new Intent(this, ClinicalHistory.class));
    }





    public void overrideFonts(final Context context, final View v) {
        try {
            if (v instanceof ViewGroup) {
                ViewGroup vg = (ViewGroup) v;
                for (int i = 0; i < vg.getChildCount(); i++) {
                    View child = vg.getChildAt(i);
                    overrideFonts(context, child);
                }
            } else if (v instanceof TextView ) {
                ((TextView) v).setTypeface(Typeface.createFromAsset(context.getAssets(), "fonts/Raleway-Semibold.ttf"));
            }
        } catch (Exception e) {
        }
    }






    }




