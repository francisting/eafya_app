package com.example.user.eafya.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.user.eafya.R;
import com.example.user.eafya.activities.MedicalDetailActivity;
import com.example.user.eafya.models.MedicalTips;

import java.util.List;



/**
 * Created by Acer on 12/4/2017.
 */

public class MedicalTipsAdapter extends RecyclerView.Adapter<MedicalTipsAdapter.myViewHolder> {
    Context ctx;
    List<MedicalTips> medicalTipsList;
    MedicalTips medicalTips;
    LayoutInflater inflater;

    public MedicalTipsAdapter(Context ctx, List<MedicalTips> medicalTipsList) {
        this.ctx = ctx;
        this.medicalTipsList = medicalTipsList;
        inflater = LayoutInflater.from(ctx);
    }

    public class myViewHolder extends RecyclerView.ViewHolder {
        TextView news_date_TV,news_title, news_desc;
        ImageView news_img;
        public myViewHolder(View itemView) {
            super(itemView);
            news_date_TV = itemView.findViewById(R.id.news_date_TV);
            news_title = itemView.findViewById(R.id.news_title__TV);
            news_desc = itemView.findViewById(R.id.news_desc_TV);
            news_img = itemView.findViewById(R.id.news_image_IV);

        }

    }

    @Override
    public MedicalTipsAdapter.myViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View my_view = inflater.inflate(R.layout.tips_item,viewGroup,false);

        return new myViewHolder(my_view);
    }

    @Override
    public void onBindViewHolder(MedicalTipsAdapter.myViewHolder myViewHolder, final int position) {
        medicalTips = new MedicalTips();
        medicalTips = medicalTipsList.get(position);

        myViewHolder.news_date_TV.setText(medicalTips.getDate());
        myViewHolder.news_title.setText(medicalTips.getTitle());
        int len = medicalTips.getDescription().length();
        String to_cut = medicalTips.getDescription().substring(0,50);


        myViewHolder.news_desc.setText(to_cut+"...READ MORE");
        myViewHolder.news_desc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goToDetails(position);
            }
        });
        Glide.with(ctx).load(medicalTips.getImg()).placeholder(R.drawable.loader).error(R.drawable.header).into(myViewHolder.news_img);



    }

    private void goToDetails(int pos) {
        medicalTips = medicalTipsList.get(pos);
        Intent intent = new Intent(ctx,MedicalDetailActivity.class);
        intent.putExtra("news_date", medicalTips.getDate());
        intent.putExtra("news_img", medicalTips.getImg());
        intent.putExtra("news_title", medicalTips.getTitle());
        intent.putExtra("news_description", medicalTips.getDescription());
        ctx.startActivity(intent);
    }

    @Override
    public int getItemCount() {
        return medicalTipsList.size();
    }
}
