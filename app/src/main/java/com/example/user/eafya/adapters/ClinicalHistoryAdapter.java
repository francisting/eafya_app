package com.example.user.eafya.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.user.eafya.R;
import com.example.user.eafya.activities.ClinicalHistoryDetailActivity;
import com.example.user.eafya.activities.MedicalDetailActivity;
import com.example.user.eafya.models.ClinicalHistory;

import java.util.List;


/**
 * Created by Acer on 12/4/2017.
 */

public class ClinicalHistoryAdapter extends RecyclerView.Adapter<ClinicalHistoryAdapter.myViewHolder> {
    Context ctx;
    List<ClinicalHistory> clinicalHistoryList;
    ClinicalHistory clinicalHistory;
    LayoutInflater inflater;

    public ClinicalHistoryAdapter(Context ctx, List<ClinicalHistory> clinicalHistoryList) {
        this.ctx = ctx;
        this.clinicalHistoryList = clinicalHistoryList;
        inflater = LayoutInflater.from(ctx);
    }

    public class myViewHolder extends RecyclerView.ViewHolder {
        TextView news_date_TV,news_title, news_desc;
        ImageView news_img;
        public myViewHolder(View itemView) {
            super(itemView);
            news_date_TV = itemView.findViewById(R.id.news_date_TV);
            news_title = itemView.findViewById(R.id.news_title__TV);
            news_desc = itemView.findViewById(R.id.news_desc_TV);
            news_img = itemView.findViewById(R.id.news_image_IV);

        }

    }

    @Override
    public ClinicalHistoryAdapter.myViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View my_view = inflater.inflate(R.layout.tips_item,viewGroup,false);

        return new myViewHolder(my_view);
    }

    @Override
    public void onBindViewHolder(ClinicalHistoryAdapter.myViewHolder myViewHolder, final int position) {
        clinicalHistory = new ClinicalHistory();
        clinicalHistory = clinicalHistoryList.get(position);

        myViewHolder.news_date_TV.setText(clinicalHistory.getDate());
        myViewHolder.news_title.setText(clinicalHistory.getTitle());
        int len = clinicalHistory.getDescription().length();
        String to_cut = clinicalHistory.getDescription().substring(0,50);


        myViewHolder.news_desc.setText(to_cut+"...READ MORE");
        myViewHolder.news_desc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goToDetails(position);
            }
        });
        Glide.with(ctx).load(clinicalHistory.getImg()).placeholder(R.drawable.loader).error(R.drawable.header).into(myViewHolder.news_img);



    }

    private void goToDetails(int pos) {
        clinicalHistory = clinicalHistoryList.get(pos);
        Intent intent = new Intent(ctx,MedicalDetailActivity.class);
        intent.putExtra("news_date", clinicalHistory.getDate());
        intent.putExtra("news_img", clinicalHistory.getImg());
        intent.putExtra("news_title", clinicalHistory.getTitle());
        intent.putExtra("news_description", clinicalHistory.getDescription());
        ctx.startActivity(intent);
    }

    @Override
    public int getItemCount() {
        return clinicalHistoryList.size();
    }
}
