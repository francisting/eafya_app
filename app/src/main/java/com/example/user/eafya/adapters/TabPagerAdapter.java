package com.example.user.eafya.adapters;

/**
 * Created by user on 08/01/2018.
 */

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.example.user.eafya.fragments.Allergies_Fragment;
import com.example.user.eafya.fragments.ChronicsFragment;

public class TabPagerAdapter extends FragmentPagerAdapter {

    int tabCount;

    public TabPagerAdapter(FragmentManager fm, int numberOfTabs) {
                super(fm);
                this.tabCount = numberOfTabs;
            }

            @Override
            public Fragment getItem(int position) {

                switch (position) {
                    case 0:
                        Allergies_Fragment tab1 = new Allergies_Fragment();
                        return tab1;
                    case 1:
                        ChronicsFragment tab2 = new ChronicsFragment();
                        return tab2;

                    default:
                        return null;
        }
    }

    @Override
    public int getCount() {
        return tabCount;
    }
}